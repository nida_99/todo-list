//selectors
const todoButton = document.querySelector(".todo-button");
const todoList = document.querySelector(".todo-list");
const deleteALl = document.querySelector(".last")


// add event listeners
todoButton.addEventListener('click', saveToLocalStorage)
deleteALl.addEventListener('click', deleteList)
todoList.addEventListener('click', checkFunction)

//Initialising an empty array
let todoItems = [];


getLocalStorage();


//save to local storage:
function saveToLocalStorage() {
    const todoInput = document.querySelector(".todo-input");
    const todo = todoInput.value;

    const currenItemIndex = document.querySelector("#updatedIndex").value;

    if (currenItemIndex) {
        updateItem(currenItemIndex, todo);
        document.querySelector("#updatedIndex").value = "";
    }
    else {
        const newObj = {
            item: todo,
            ischecked: false,
            addedAt: new Date().getTime()
        }
        todoItems.push(newObj)
    }
    localStorage.setItem('todoItem', JSON.stringify(todoItems))
    getList(todoItems);
}


function getLocalStorage() {
    const storage = localStorage.getItem('todoItem');
    if (storage === null || storage === undefined) {
        todoItems = [];
    }
    else {
        todoItems = JSON.parse(storage);
    }
    getList(todoItems);
}


function getList(todoItems) {
    const todoInput = document.querySelector(".todo-input");

    if (todoItems.length > 0) {


        todoList.innerText = '';

        todoItems.forEach((todo) => {
            const newDiv = document.createElement('div');
            newDiv.classList.add('todo-div')

            newDiv.classList.add(todo.addedAt)


            if (todo.ischecked === true) {
                newDiv.classList.toggle('completedTask')
            }
            else {
                newDiv.classList.remove('completedTask');
            }

            //create li
            const newItem = document.createElement('li');
            newItem.classList.add('todo-item')

            //adding inner-text
            newItem.innerText = todo.item;

            //append li to div :
            newDiv.appendChild(newItem);

            //checked button 

            const checkedButton = document.createElement('button');
            checkedButton.innerHTML = '<i class="fas fa-check-square"></i>'
            checkedButton.classList.add('checked-button')
            newDiv.appendChild(checkedButton);

            //delete button

            const deleteButton = document.createElement('button');
            deleteButton.innerHTML = '<i class="fas fa-trash"></i>'
            deleteButton.classList.add('delete-button')
            newDiv.appendChild(deleteButton);

            //edit button 

            const editButton = document.createElement('button');
            editButton.innerHTML = '<i class="fas fa-pen-square"></i>'
            editButton.classList.add('edit-button')
            newDiv.appendChild(editButton);

            //appending to ul 
            todoList.appendChild(newDiv);

            //clear input value 
            todoInput.value = '';
        });
    }

}

function checkFunction(event) {
    const todoInput = document.querySelector(".todo-input");

    const target = event.target;
    const toDelete = target.parentElement;

    if (target.classList[0] === "delete-button") {
        removeFromLocalStorage(toDelete)
        toDelete.remove();
    }

    if (target.classList[0] === "checked-button") {
        const toCheck = target.parentElement;
        toCheck.classList.toggle('completedTask')
        const tar = toCheck.classList[1];
        checkTask(tar)
    }


    if (target.classList[0] === "edit-button") {
        const tar = target.parentElement;
        let todos = JSON.parse(localStorage.getItem('todoItem'));
        const editId = tar.classList[1]
        const oldValue = target.parentElement.children[0].innerText;

        todoInput.value = oldValue;
        const index = todos.findIndex((ele, idx) => {
            const id = todos[idx].addedAt;
            if (id == editId) {

                return true;
            }
        })
        const updated = document.querySelector("#updatedIndex");
        updated.value = index;

    }

}
function updateItem(itemIndex, newValue) {
    const newItem = todoItems[itemIndex];
    newItem.item = newValue;
    todoItems.splice(itemIndex, 1, newItem);
    localStorage.setItem("todoItem", JSON.stringify(todoItems));
};


function checkTask(tar) {
    let todos = JSON.parse(localStorage.getItem('todoItem'));
    const index = todos.findIndex((ele, idx) => {
        const id = todos[idx].addedAt;
        if (id == tar) {
            return true;
        }
    })


    if (todos[index].ischecked == true) {
        todos[index].ischecked = false

    } else {
        todos[index].ischecked = true;
    }

    localStorage.setItem('todoItem', JSON.stringify(todos))

}
function removeFromLocalStorage(toDelete) {

    let todos = JSON.parse(localStorage.getItem('todoItem'));
    const index = todos.findIndex((ele, idx) => {
        const id = todos[idx].addedAt;
        if (id == toDelete.classList[1]) {
            return true;
        }
    })
    todos.splice(index, 1);
    localStorage.setItem('todoItem', JSON.stringify(todos));
    getLocalStorage();

}

function deleteList() {

    todoList.innerText = '';
    let emptyArr = [];
    todoItems = emptyArr;
    localStorage.removeItem('todoItem')
}

